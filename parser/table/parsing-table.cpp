#include "parsing-table.h"
#include "../../utils/debug.h"

#pragma ide diagnostic ignored "performance-unnecessary-value-param"

ParsingTable::ParsingTable() = default;

ParsingTable::ParsingTable(unordered_map<string, Token *> &terminals,
                           unordered_map<string, Production *> &non_terminals) {
    this->rows = non_terminals;

    unordered_set<string> symbols;
    for (auto &symbol : non_terminals)
        for (auto &rule : symbol.second->getSyntax())
            for (auto &term : rule)
                symbols.insert(term);

    for (auto &symbol : non_terminals) {
        symbols.erase(symbol.first);
        this->table[symbol.first] = unordered_map<string, vector<string>>();
    }
    for (auto &symbol : terminals) {
        symbols.erase(symbol.first);
        this->columns[symbol.first] = symbol.second;
    }

    for (auto &symbol : symbols) {
        if (symbol == EPSILON) continue;

        auto s = new Symbol();
        s->name = findSuperToken(symbol, terminals);
        if (s->name.empty()) {
            cerr << "Parser error: undefined symbol '" << symbol << "' can't be matched!" << endl;
            continue;
        }
        cerr << "Parser warning: undefined symbol " << symbol << " will be treated as '" << s->name << "' token"
             << endl;
        this->columns[symbol] = s;

    }

    this->columns[$_EOF] = new Symbol($_EOF);
    initializeTable();

}

#define IS_TERMINAL(symbol) columns.find(symbol) != columns.end()
#define IS_NON_TERMINAL(symbol) rows.find(symbol) != rows.end()

bool ParsingTable::matchInput(string &symbol, Token &input) {
    return IS_TERMINAL(symbol) && columns[symbol]->name == input.name;
}

vector<string> ParsingTable::getProductionRule(string &production, string &input) {
    return this->table[production][input];
}

void ParsingTable::initializeTable() {
    for (auto &symbol : rows) {
        symbol.second->first_terms = identifyFirst(symbol.first);
        symbol.second->follow_terms = identifyFollow(symbol.first);
        fillTableEntry(symbol.first);
    }
    // DEBUG
    Debug::printFFTable(rows);
    Debug::printLLTable(columns, table);
}

void ParsingTable::fillTableEntry(string symbol_name) {
    auto symbol = rows[symbol_name];
    for (auto &s : symbol->first_terms) {
        if (s == EPSILON) continue;
        for (auto &rule : symbol->getSyntax()) {
            if (rule[0] == s)
                this->table[symbol->name][rule[0]] = rule;
            if (IS_NON_TERMINAL(rule[0]) && rows[rule[0]]->startsWith(s))
                for (auto &term : rows[rule[0]]->first_terms)
                    this->table[symbol->name][term] = rule;
        }
    }

    if (symbol->hasEpsilon())
        for (auto &s : symbol->follow_terms)
            this->table[symbol->name][s] = {EPSILON};
}

unordered_set<string> ParsingTable::identifyFirst(string symbol_name) {
    auto symbol = rows[symbol_name];
    if (!symbol->first_terms.empty())
        return symbol->first_terms;

    unordered_set<string> first;
    for (auto &rule : symbol->getSyntax())
        if (IS_TERMINAL(rule[0]))
            first.insert(rule[0]);
        else if (IS_NON_TERMINAL(rule[0]))
            // TODO: consider generalized later.
            for (auto &term : identifyFirst(rule[0]))
                first.insert(term);
        else
            first.insert(EPSILON);

    symbol->first_terms = first;
    return first;
}

unordered_set<string> ParsingTable::identifyFollow(string symbol_name) {
    auto symbol = rows[symbol_name];
    if (!symbol->follow_terms.empty())
        return symbol->follow_terms;

    unordered_set<string> follow;
    for (auto &other_symbol : rows)
        for (auto &rule : other_symbol.second->getSyntax())
            for (int i = 0; i < rule.size(); i++)
                if (rule[i] == symbol_name) {
                    if (i == rule.size() - 1)
                        if (other_symbol.first == symbol_name)
                            continue;
                        else
                            for (auto &term : identifyFollow(other_symbol.first))
                                follow.insert(term);
                    else if (IS_TERMINAL(rule[i + 1]))
                        follow.insert(rule[i + 1]);
                    else if (IS_NON_TERMINAL(rule[i + 1]))
                        for (auto &term : identifyFirst(rule[i + 1]))
                            if (term != EPSILON)
                                follow.insert(term);

                    //TODO: can be generalized later.
                    if ((i == rule.size() - 2 && IS_NON_TERMINAL(rule[i + 1]) && rows[rule[i + 1]]->hasEpsilon()))
                        for (auto &term : identifyFollow(other_symbol.first))
                            follow.insert(term);
                }
    follow.insert($_EOF);

    symbol->follow_terms = follow;
    return follow;
}

string ParsingTable::findSuperToken(string sub_token, unordered_map<string, Token *> tokens) {
    for (auto &t : tokens) {
        for (auto c : sub_token)
            t.second->checkChar(c);
        auto matched = t.second->isMatched();
        t.second->clearLexeme();
        if (matched)
            return t.first;
    }
    return "";
}
