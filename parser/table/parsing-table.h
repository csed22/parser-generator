#ifndef PHASE2_PARSER_PARSING_TABLE_H
#define PHASE2_PARSER_PARSING_TABLE_H

#include <unordered_map>
#include "../production.h"
#include "../../tokenizer/token.h"

class ParsingTable {
public:
    ParsingTable();

    explicit ParsingTable(unordered_map<string, Token *> &terminals,
                          unordered_map<string, Production *> &non_terminals);

    bool matchInput(string &symbol, Token &input);

    vector<string> getProductionRule(string &production, string &input);

private:
    unordered_map<string, Symbol *> columns;
    unordered_map<string, Production *> rows;
    unordered_map<string, unordered_map<string, vector<string>>> table;

    void initializeTable();

    void fillTableEntry(string symbol_name);

    unordered_set<string> identifyFirst(string symbol_name);

    unordered_set<string> identifyFollow(string symbol_name);

    static string findSuperToken(string sub_token, unordered_map<std::string, Token *> tokens);

};

#endif //PHASE2_PARSER_PARSING_TABLE_H
