#ifndef PHASE2_PARSER_PRODUCTION_H
#define PHASE2_PARSER_PRODUCTION_H

#include <vector>
#include <unordered_set>
#include "symbol.h"

// A PRODUCTION has SYNTAX of one/many RULES containing TERMS.
class Production : public Symbol {
public:
    unordered_set<string> first_terms;
    unordered_set<string> follow_terms;

    explicit Production(string &production);

    bool hasEpsilon();

    bool startsWith(string term);

    vector<vector<string>> getSyntax();

    Production *eliminateLeftRecursion();

    Production *leftFactor();

    bool hasCommonLeftFactor();

private:
    vector<vector<string>> syntax;

    Production(string &name, vector<vector<string>> &syntax);

    bool hasLeftRecursion();


};

#endif //PHASE2_PARSER_PRODUCTION_H
