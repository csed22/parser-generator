#include <regex>
#include "production.h"

Production::Production(string &production) {

    regex white_space("\\s+");
    regex trailing_white_space("\\s+$");
    smatch match_rules;

    regex syntax_rule(R"((([A-Z_]+|'[^A-Z\s]+'|\\L)\s*)+)");
    regex_search(production, match_rules, syntax_rule);

    vector<string> syntax_rules;

    string::const_iterator production_itr(production.cbegin());
    while (regex_search(production_itr, production.cend(), match_rules, syntax_rule)) {
        auto str = regex_replace(regex_replace(match_rules[0].str(), white_space, " "), trailing_white_space, "");
        if (this->name.empty())
            this->name = str;
        else
            syntax_rules.push_back(str);
        production_itr = match_rules.suffix().first;
    }

    for (auto &rule : syntax_rules) {
        smatch match_terms;
        vector<string> terms;
        regex rule_term(R"([^'\s]+)");

        string::const_iterator term_itr(rule.cbegin());
        while (regex_search(term_itr, rule.cend(), match_terms, rule_term)) {
            terms.push_back(match_terms[0]);
            term_itr = match_terms.suffix().first;
        }

        this->syntax.push_back(terms);
    }

}

bool Production::hasEpsilon() {
    for (auto &rule : syntax)
        for (auto &term : rule)
            if (term == EPSILON)
                return true;
    return false;
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "performance-unnecessary-value-param"

bool Production::startsWith(string term) {
    return first_terms.find(term) != first_terms.end();
}

#pragma clang diagnostic pop

vector<vector<string>> Production::getSyntax() {
    return syntax;
}

/**
 *  if:     A  -> A alpha | beta
 * @return  A  -> beta A_
 *          A_ -> alpha A_ | EPSILON
 */
Production *Production::eliminateLeftRecursion() {
    if (!hasLeftRecursion())
        return this;

    // A -> A alpha | beta
    vector<string> alpha;
    vector<vector<string>> beta;
    for (auto &rule : syntax) {
        if (rule[0] == name) {
            rule.erase(rule.begin());
            alpha = rule;
        } else {
            beta.push_back(rule);
        }
    }

    // A_ -> alpha A_ | EPSILON
    auto name_dash = name + "_";
    alpha.push_back(name_dash);
    vector<vector<string>> syntax_dash;
    syntax_dash.push_back(alpha);
    syntax_dash.push_back({EPSILON});

    // A -> beta A_
    syntax.clear();
    for (auto &rule : beta) {
        rule.push_back(name_dash);
        syntax.push_back(rule);
    }

    return new Production(name_dash, syntax_dash);
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-narrowing-conversions"

/**
 *  if:     A   -> alpha beta_1 | alpha beta_2
 * @return  A   -> alpha A__
 *          A__ -> beta_1 | beta_2
 */
Production *Production::leftFactor() {
    if (!hasCommonLeftFactor())
        return this;

    // A -> alpha beta_1 | alpha beta_2
    vector<string> alpha;
    vector<vector<string>> betas;

    set_intersection(syntax[0].begin(), syntax[0].end(), syntax[1].begin(), syntax[1].end(),
                     back_inserter(alpha));
    for (auto &rule : syntax)
        if (rule.size() > alpha.size())
            betas.emplace_back(rule.begin() + alpha.size(), rule.end());
        else
            betas.push_back({EPSILON});

    // A__ -> beta_1 | beta_2
    auto name_dash_dash = name + "__";

    // A -> alpha A__
    syntax.clear();
    alpha.push_back(name_dash_dash);
    syntax.push_back(alpha);

    return new Production(name_dash_dash, betas);
}

#pragma clang diagnostic pop

Production::Production(string &name, vector<vector<string>> &syntax) {
    this->name = name;
    this->syntax = syntax;
}

bool Production::hasLeftRecursion() {
    for (auto &rule : syntax)
        if (rule[0] == name)
            return true;
    return false;
}

bool Production::hasCommonLeftFactor() {
    for (int i = 0; i < syntax.size(); i++)
        for (int j = i + 1; j < syntax.size(); j++)
            if (syntax[i][0] == syntax[j][0])
                return true;
    return false;
}
