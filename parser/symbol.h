#ifndef PHASE2_PARSER_SYMBOL_H
#define PHASE2_PARSER_SYMBOL_H

#define $_EOF "$_EOF"
#define EPSILON "\\L"

using namespace std;

class Symbol {
public:
    string name;

    Symbol() = default;

    explicit Symbol(string name) { this->name = name; }
};

#endif //PHASE2_PARSER_SYMBOL_H
