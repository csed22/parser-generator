#include <regex>
#include "parsing-rules.h"
#include "../utils/debug.h"
#include "../utils/file-reader.h"

__attribute__((unused)) ParsingRules::ParsingRules(string &rules_file,
                                                   unordered_map<string, Token *> &terminals) {

    regex split_rule(R"(#\s?[A-Z_]+\s?=(\s*([A-Z_]+|'[^A-Z\s]+'|\\L)\s*\|?)+)");
    auto rules = FileReader::loadFile(rules_file, split_rule);

    regex white_space("\\s+");
    for (auto &rule : rules) {
        auto syntax = regex_replace(rule, white_space, " ");
        auto p = Production(syntax);
        if (this->firstSymbol.empty())
            this->firstSymbol = p.name;
        this->symbols[p.name] = new Production(p);
        // DEBUG
        Debug::addParsingRule(this->symbols[p.name]);
    }

    unordered_map<string, Production *> symbols_dash;
    for (auto &symbol : this->symbols) {
        symbols_dash[symbol.first] = symbol.second;

        auto symbol_dash = symbol.second->eliminateLeftRecursion();
        symbols_dash[symbol_dash->name] = symbol_dash;
        // DEBUG
        if (symbol.first != symbol_dash->name)
            Debug::addBetterParsingRule(symbol.second, symbol_dash);

        auto symbol_dash_dash = symbol.second->leftFactor();
        symbols_dash[symbol_dash_dash->name] = symbol_dash_dash;
        // DEBUG
        if (symbol.first != symbol_dash_dash->name)
            Debug::addBetterParsingRule(symbol.second, symbol_dash_dash);
    }

    if (this->symbols.size() != symbols_dash.size())
        this->symbols = symbols_dash;

    this->table = ParsingTable(terminals, symbols);

}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-narrowing-conversions"

bool ParsingRules::consumeToken(Token token) {

    static bool failed = false;

    if (stack.empty() && !firstSymbol.empty()) {
        stack.push_back(firstSymbol);
        firstSymbol.clear();
        // DEBUG
        Debug::initStackSteps();
    }

    auto matched = false;
    while (!matched && !stack.empty()) {

        for (int i = stack.size() - 1; i >= 0; i--)
            cout << stack[i] << " ";
        cout << endl;

        auto symbol = stack.back();
        // DEBUG
        if (symbol != EPSILON)
            Debug::addStackStep(stack);

        stack.pop_back();
        if (symbol == EPSILON)
            continue;

        // DEBUG
        string debug_status;

        if (table.matchInput(symbol, token)) {
            matched = true;
            // DEBUG
            debug_status = "matched";
        } else {
            auto sub = table.getProductionRule(symbol, token.name);
            for (int i = sub.size() - 1; i >= 0; i--)
                stack.push_back(sub[i]);

            if (sub.empty()) {
                failed = true;
                if (!Debug::isDebug())
                    break;
            }

            // DEBUG
            debug_status = symbol;
            PRODUCTION_AS_STRING(status, sub);
            if (status.empty())
                debug_status += " -> failed to match";
            else
                debug_status += " -> " + status;
        }
        // DEBUG
        if (symbol != EPSILON)
            Debug::finishStackStep(token.name, debug_status);
    }

    if (!Debug::isDebug() && failed) {
        cout << "reject";
        return false;
    } else if (token.name == $_EOF) {
        cout << "accept";
        return false;
    }

    return true;
}

#pragma clang diagnostic pop
