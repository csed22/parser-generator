#ifndef PHASE2_PARSER_PARSING_RULES_H
#define PHASE2_PARSER_PARSING_RULES_H

#include "table/parsing-table.h"

class ParsingRules {
public:
    unordered_map<string, Production *> symbols;

    explicit ParsingRules(string &rules_file, unordered_map<string, Token *> &terminals);

    // To separate the parsing rules logic from main program.
    bool consumeToken(Token token);

private:
    ParsingTable table;
    string firstSymbol;
    vector<string> stack;

};

#endif //PHASE2_PARSER_PARSING_RULES_H
