#include <iostream>
#include "utils/debug.h"
#include "parser/parsing-rules.h"
#include "tokenizer/lexical-rules.h"

#define DEBUG_MODE 0
#define LEXICAL_MODE 1
#define PARSING_MODE 2

static string input;
static string lexical_rules;
static string parsing_rules;

int identifyMode(int argc, char **argv);

int main(int argc, char *argv[]) {

    auto mode = identifyMode(argc, argv);
    if (mode == DEBUG_MODE)
        Debug::activate();

    LexicalRules lex(lexical_rules, input);

    if (mode == LEXICAL_MODE) {
        auto token = lex.produceToken();
        while (token.name != $_EOF) {
            cout << token.name << endl;
            token = lex.produceToken();
        }
    } else {
        ParsingRules parse(parsing_rules, lex.tokens);
        while (parse.consumeToken(lex.produceToken()));
    }

    return 0;
}

#define CONTAINS(c_str, search) string(c_str).find(#search) != string::npos
#define CHECK_FLAGS \
    if (CONTAINS(argv[1], -lex))\
        return LEXICAL_MODE;\
    else if (CONTAINS(argv[1], -debug))\
        return DEBUG_MODE

int identifyMode(int argc, char **argv) {

    input = "simple-input.txt";
    lexical_rules = "lexical-rules.txt";
    parsing_rules = "simple-parsing-rules.txt";

    switch (argc) {
        case 2:
            CHECK_FLAGS;
            else
                input = string(argv[1]);
            break;
        case 3:
            input = string(argv[2]);
            CHECK_FLAGS;
            break;
        case 4:
            if (CONTAINS(argv[1], -lex)) {
                input = string(argv[2]);
                lexical_rules = string(argv[3]);
                return LEXICAL_MODE;
            }
            input = string(argv[1]);
            lexical_rules = string(argv[2]);
            parsing_rules = string(argv[3]);
            break;
        case 5:
            input = string(argv[2]);
            lexical_rules = string(argv[3]);
            parsing_rules = string(argv[4]);
            CHECK_FLAGS;
            break;
        default:
            return PARSING_MODE;
    }

    return PARSING_MODE;
}
