#include "state-machine.h"

StateMachine::StateMachine() = default;

StateMachine::StateMachine(vector<Token> &punctuations, vector<Token> &keywords, vector<Token> &expressions) {
    this->rules[0] = expressions;
    this->rules[1] = keywords;
    this->rules[2] = punctuations;
}

int StateMachine::readCharacter(char input) {

    // Handle blank space after an accepted lexeme.
    if (prev_accepted && isspace(input))
        return 0;

    vector<Token> currentStates;

    // Handle preceding punctuation produceToken.
    if (acceptedStates.empty())
        for (auto &punc : rules[2])
            if (punc.checkChar(input) == MATCH) {
                acceptedStates.push_back(punc);
                goto CheckStates;
            }

    // Only exclude Trapped states.
    if (!isspace(input)) {
        prev_accepted = false;
        for (auto &rule : rules)
            for (auto &token : rule)
                if (token.checkChar(input) != TRAP)
                    currentStates.push_back(token);
    }

    // Handle middle/trailing punctuation produceToken.
    {
        auto separated = separateTailingPunctuation();
        if (separated.empty())
            goto CheckStates;
        acceptedStates = separated;
        double_output = true;
        return 2;
    }

    CheckStates:
    if (currentStates.empty())
        return 1;

    acceptedStates = currentStates;

    return 0;
}

Token StateMachine::getOutput() {

    Token output("", {}, true);

    // Handle middle/trailing punctuation produceToken.
    if (double_output && acceptedStates.empty()) {
        double_output = false;
        return punctuation_token;
    }

    // Highest priority matched token will override what is before it
    for (auto &token : acceptedStates)
        if (token.isMatched())
            output = token;

    if (output.lexeme.empty())
        output.lexeme = acceptedStates[0].lexeme;
    else
        prev_accepted = true;

    acceptedStates.clear();
    resetTokens();

    return output;
}

void StateMachine::resetTokens() {
    for (auto &rule : rules)
        for (auto &token : rule)
            token.clearLexeme();
}

vector<Token> StateMachine::separateTailingPunctuation() {
    vector<Token> currentStates;
    for (auto &punc : rules[2]) {
        char c = punc.lexeme.back();
        punc.clearLexeme();
        if (punc.checkChar(c) == MATCH) {
            punctuation_token = punc;
            for (auto &rule : rules)
                for (auto &token : rule) {
                    token.truncateLexeme(1);
                    if (token.checkChar('\0') != TRAP && token.lexeme != punc.lexeme)
                        currentStates.push_back(token);
                }
            return currentStates;
        }
    }
    return vector<Token>();
}
