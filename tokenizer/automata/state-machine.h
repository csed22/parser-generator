#ifndef PHASE1_LEXICAL_STATE_MACHINE_H
#define PHASE1_LEXICAL_STATE_MACHINE_H

#include "../token.h"

class StateMachine {
public:
    StateMachine();

    explicit StateMachine(vector<Token> &punctuations, vector<Token> &keywords, vector<Token> &expressions);

    int readCharacter(char input);

    Token getOutput();

private:
    vector<Token> rules[3];
    vector<Token> acceptedStates;

    void resetTokens();

    bool prev_accepted = false;
    bool double_output = false;
    Token punctuation_token = Token("", {}, true);

    vector<Token> separateTailingPunctuation();
};

#endif //PHASE1_LEXICAL_STATE_MACHINE_H
