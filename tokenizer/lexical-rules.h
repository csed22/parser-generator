#ifndef PHASE1_LEXICAL_LEXICAL_RULES_H
#define PHASE1_LEXICAL_LEXICAL_RULES_H

#include <unordered_map>
#include "../utils/file-reader.h"
#include "automata/state-machine.h"

class LexicalRules {
public:
    unordered_map<string, Token *> tokens;

    explicit LexicalRules(string &rules_file, string &input_file);

    // To separate the lexical rules logic from main program.
    Token produceToken();

private:
    StateMachine sm;
    FileReader input;
    int available_tokens;
    bool error_flag = false;

    StateMachine generateStateMachine(string &rules_file);

    Token nextToken();

    bool isError();

    vector<int> errorLocation(string &false_lexeme);

    static string convertToRegex(string &rule, bool without_definitions);

    static vector<string> separateRules(string &rules);
};

#endif //PHASE1_LEXICAL_LEXICAL_RULES_H
