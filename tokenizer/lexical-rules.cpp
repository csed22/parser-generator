#include "lexical-rules.h"
#include "../utils/debug.h"

#pragma clang diagnostic push
#pragma ide diagnostic ignored "OCDFAInspection"

LexicalRules::LexicalRules(string &rules_file, string &input_file) {
    this->sm = generateStateMachine(rules_file);
    this->input = FileReader(input_file);
    this->available_tokens = 0;
}

Token LexicalRules::produceToken() {

    auto t = nextToken();
    if (!(t.name.empty() && !isError())) {
        if (t.name.empty()) {
            cerr << "Lexical warning: undefined token " << errorLocation(t.lexeme)[0] << ':'
                 << errorLocation(t.lexeme)[1] << '\t' << t.lexeme << endl;
            t.name = "undefined";
        }
        // DEBUG
        Debug::addToken(t);
        return t;
    }

    return Token($_EOF, {}, true);
}

// https://stackoverflow.com/questions/18831470/sorting-a-string-vector-based-on-the-string-size
struct compare {
    inline bool operator()(const std::string &first,
                           const std::string &second) const {
        return first.size() > second.size();
    }
} compare_size;

vector<string> matchResults(string str, regex regex);

StateMachine LexicalRules::generateStateMachine(string &rules_file) {

    regex split_rule(R"([^\n]+)");
    auto rules = FileReader::loadFile(rules_file, split_rule);

    vector<Token> punctuations;
    regex punctuation_rule(R"(\[.*\])");

    vector<Token> keywords;
    regex keyword_rule(R"(\{.+\})");

    vector<Token> expressions;
    regex expression_rule(R"([a-z]+:[^\n]+)");

    vector<string> definitions_order;
    unordered_map<string, string> definitions;
    regex definition_rule(R"([a-z]+=[^\n]+)");

    for (auto &rule : rules) {
        if (regex_match(rule, punctuation_rule)) {
            auto punc = matchResults(rule.substr(1, rule.size() - 2), regex(R"([^\\\s])"));
            for (auto &p : punc)
                punctuations.push_back(Token(p, {"\\" + p}, true));
        } else if (regex_match(rule, keyword_rule)) {
            auto keyword = matchResults(rule.substr(1, rule.size() - 2), regex(R"(\S+)"));
            for (auto &k : keyword)
                keywords.push_back(Token(k, {k}, true));
        } else {
            auto regex_rule = convertToRegex(rule, false);
            if (regex_match(regex_rule, definition_rule)) {
                auto def = matchResults(regex_rule, regex(R"([^=]+)"));
                definitions_order.push_back(def[0]);
                std::sort(definitions_order.begin(), definitions_order.end(), compare_size);
                auto def_rule = def[1];
                for (auto &other_def_rule : definitions_order) {
                    def_rule = regex_replace(def_rule, regex(other_def_rule), definitions[other_def_rule]);
                }
                definitions[def[0]] = def_rule;
            } else if (regex_match(regex_rule, expression_rule)) {
                auto exp = matchResults(regex_rule, regex(R"([^:]+)"));
                auto exp_rules = exp[1];
                for (auto &other_def_rule : definitions_order) {
                    exp_rules = regex_replace(exp_rules, regex(other_def_rule),
                                              "(" + definitions[other_def_rule] + ")");
                }
                if (exp_rules == exp[1])
                    exp_rules = convertToRegex(exp_rules, true);

                expressions.emplace_back(exp[0], separateRules(exp_rules), false);
            }
        }
    }

    for (auto &t : punctuations)
        tokens[t.name] = new Token(t);
    for (auto &t : keywords)
        tokens[t.name] = new Token(t);
    for (auto &t : expressions)
        tokens[t.name] = new Token(t);

    return StateMachine(punctuations, keywords, expressions);
}

Token LexicalRules::nextToken() {
    while (!available_tokens) {
        char c = input.getChar();
        if (!c) return Token("", {}, true);

        available_tokens = sm.readCharacter(c);
    }

    auto token = sm.getOutput();
    if (token.name.empty())
        error_flag = true;

    available_tokens--;
    return token;
}

bool LexicalRules::isError() {
    if (error_flag) {
        error_flag = false;
        return true;
    }
    return false;
}

#pragma clang diagnostic push
#pragma ide diagnostic ignored "performance-unnecessary-value-param"

vector<string> matchResults(string str, regex regex) {
    smatch match_results;
    regex_search(str, match_results, regex);

    vector<string> results;
    string::const_iterator str_itr(str.cbegin());
    while (regex_search(str_itr, str.cend(), match_results, regex)) {
        results.push_back(match_results[0].str());
        str_itr = match_results.suffix().first;
    }

    return results;
}

#pragma ide diagnostic ignored "cppcoreguidelines-narrowing-conversions"

vector<int> LexicalRules::errorLocation(string &false_lexeme) {
    auto location = input.charLocation();
    location[1] -= (false_lexeme.size() + 1);
    return location;
}

string LexicalRules::convertToRegex(string &rule, bool without_definitions) {

    regex epsilon(EPSILON);
    regex back_slash(R"(\\)");
    regex white_space(R"(\s+)");
    auto regex_rule = regex_replace(rule, white_space, "");
    regex_rule = regex_replace(regex_rule, epsilon, "EPSILON");
    regex_rule = regex_replace(regex_rule, back_slash, "");

    smatch match;
    regex r(R"(.-.)");
    auto str = regex_rule;

    auto prev_position = 0;
    while (regex_search(str, match, r)) {

        if (prev_position)
            prev_position = prev_position + 5 + match.position(0);
        else
            prev_position = match.position(0);

        regex_rule.insert(prev_position, "[");
        regex_rule.insert(prev_position + 4, "]");

        str = match.suffix().str();
    }

    if (without_definitions)
        for (int i = 0; i < regex_rule.size(); i++)
            if (!isdigit(regex_rule[i]) && !isalpha(regex_rule[i]) && regex_rule[i] != '|') {
                regex_rule.insert(i, "\\");
                i++;
            }

    // SPECIAL CASES
    if (regex_rule.find('.') != string::npos)
        regex_rule = regex_rule.insert(regex_rule.find('.'), "\\");

    str = regex_rule;
    regex with_epsilon(R"(\(EPSILON\|.+\))");
    if (regex_search(regex_rule, with_epsilon)) {
        while (regex_search(str, match, with_epsilon)) {
            int unmatched = 0;
            for (int i = match.position(0) + 1; i < regex_rule.size(); i++) {
                if (regex_rule[i] == '(')
                    unmatched++;
                else if (regex_rule[i] == ')')
                    unmatched--;

                if (unmatched < 0) {
                    regex_rule.insert(i + 1, "?");
                    break;
                }
            }
            str = match.suffix().str();
        }
        regex_rule = regex_replace(regex_rule, regex(R"(EPSILON\|)"), "");
    }

    return regex_rule;
}

vector<string> LexicalRules::separateRules(string &rules) {
    if (!regex_search(rules, regex(R"(\|)")))
        return {rules};

    int unmatched_brackets = 0;
    vector<int> separations;

    for (int i = 0; i < rules.size(); i++) {
        if (rules[i] == '(')
            unmatched_brackets++;
        else if (rules[i] == ')')
            unmatched_brackets--;
        else if (rules[i] == '|') {
            if (unmatched_brackets)
                continue;
            else
                separations.push_back(i);
        }
    }

    if (separations.empty())
        return {rules};

    vector<string> separated_rules;
    auto prev_separator = 0;
    for (auto &separator : separations) {
        separated_rules.push_back(rules.substr(prev_separator, separator - prev_separator));
        prev_separator = separator + 1;
    }
    separated_rules.push_back(rules.substr(prev_separator));

    return separated_rules;
}

#pragma clang diagnostic pop
