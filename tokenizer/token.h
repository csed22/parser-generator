#ifndef PHASE1_LEXICAL_TOKEN_H
#define PHASE1_LEXICAL_TOKEN_H

#include <regex>
#include "../parser/symbol.h"

#define size_type unsigned long long

enum MatchState {
    TRAP = 0, PENDING = 1, MATCH = 2
};

class Token : public Symbol {
public:
    string lexeme;

    Token();

    Token(string name, vector<string> patterns, bool limited_size);

    MatchState checkChar(char c);

    bool isMatched() const;

    void clearLexeme();

    void truncateLexeme(int trunc);

private:
    bool matched{};
    size_type max_size{};
    vector<regex> regex_patterns;

    string CheckLexeme(char c);
};

#undef size_type
#endif //PHASE1_LEXICAL_TOKEN_H
