#include <fstream>
#include "file-reader.h"

#pragma ide diagnostic ignored "cppcoreguidelines-pro-type-member-init"

FileReader::FileReader() = default;

FileReader::FileReader(string &file_path) {
    this->file = loadFile(file_path);
    this->i = 0;
    this->row = 1;
    this->col = 1;
}

char FileReader::getChar() {
    if (i < file.size()) {
        char c = file[i];
        i++, col++;
        if (c == '\n') {
            row++;
            col = 1;
        }
        return c;
    }
    return '\0';
}

vector<int> FileReader::charLocation() {
    return {row, col};
}

vector<string> FileReader::loadFile(string &file_path, regex &regex) {

    auto str = loadFile(file_path);

    smatch match_results;
    regex_search(str, match_results, regex);

    vector<string> results;
    string::const_iterator str_itr(str.cbegin());
    while (regex_search(str_itr, str.cend(), match_results, regex)) {
        results.push_back(match_results[0].str());
        str_itr = match_results.suffix().first;
    }

    return results;
}

string FileReader::loadFile(string &file_path) {
    ifstream fs(file_path);
    string str((istreambuf_iterator<char>(fs)),
               istreambuf_iterator<char>());
    fs.close();
    return str[str.size() - 1] == '\n' ? str : str + "\n";
}
