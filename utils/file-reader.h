#ifndef PHASE1_LEXICAL_FILE_READER_H
#define PHASE1_LEXICAL_FILE_READER_H

#include <string>
#include <vector>
#include <regex>

#define size_type unsigned long long

using namespace std;

class FileReader {
public:
    FileReader();

    explicit FileReader(string &file_path);

    char getChar();

    vector<int> charLocation();

    static vector<string> loadFile(string &file_path, regex &regex);

private:
    string file;
    size_type i;
    int row, col;

    static string loadFile(string &file_path);
};

#undef size_type
#endif //PHASE1_LEXICAL_FILE_READER_H
