#include <sys/stat.h>
#include "debug.h"

#define DEBUG if(!debug) return

static bool debug = false;


bool Debug::isDebug() { return debug; }

static FILE *lexical_tokens;
static FILE *parsing_rules;
static FILE *ff_table;
static FILE *ll_table;
static FILE *stack_steps;

void Debug::activate() {

    mkdir("debug");
    debug = true;

    lexical_tokens = fopen("debug/lexical-tokens.txt", "w");
    parsing_rules = fopen("debug/parsing-rules.txt", "w");
    ff_table = fopen("debug/ff-table.tsv", "w");
    ll_table = fopen("debug/ll-table.tsv", "w");
    stack_steps = fopen("debug/stack-steps.tsv", "w");

}

static vector<string> production_order;

void Debug::addParsingRule(Production *production) {
    DEBUG;
    string log = "# " + production->name + " =";
    for (auto &rule : production->getSyntax()) {
        for (auto &term : rule)
            log += " " + term;
        log += " |";
    }
    log[log.size() - 2] = '\n';
    log[log.size() - 1] = '\0';
    fprintf(parsing_rules, "%s", log.c_str());

    if (find(production_order.begin(), production_order.end(), production->name) == production_order.end())
        production_order.push_back(production->name);
}

void Debug::addBetterParsingRule(Production *production, Production *production_dash) {
    DEBUG;
    fprintf(parsing_rules, "\n");
    Debug::addParsingRule(production);
    Debug::addParsingRule(production_dash);
}

void Debug::addToken(Token &token) {
    DEBUG;
    fprintf(lexical_tokens, "%s\n", token.name.c_str());
}

#define TERMS_AS_STRING(str) \
    string str;\
    for (auto &term : productions[p]->str##_terms)\
        (str) += " " + term;\
    (str) = (str).substr(1);\
    (str) = regex_replace((str), regex("\\$_EOF"), "$")

void Debug::printFFTable(unordered_map<string, Production *> &productions) {
    DEBUG;
    fprintf(ff_table, "symbol\tfirst\tfollow\n");
    for (auto &p : production_order) {
        TERMS_AS_STRING(first);
        TERMS_AS_STRING(follow);
        fprintf(ff_table, "%s\t%s\t%s\n", p.c_str(), first.c_str(), follow.c_str());
    }
}

void Debug::printLLTable(unordered_map<string, Symbol *> &columns,
                         unordered_map<string, unordered_map<string, vector<string>>> &values) {
    DEBUG;
    vector<string> columns_order;
    columns_order.reserve(columns.size());
    for (auto &column : columns)
        columns_order.push_back(column.first);

    sort(columns_order.begin(), columns_order.end());
    auto symbol = columns_order[0];
    columns_order.erase(columns_order.begin());
    columns_order.push_back(symbol);

    fprintf(ll_table, "symbol");
    for (auto &column : columns_order)
        fprintf(ll_table, "\t%s", regex_replace((column), regex("\\$_EOF"), "$").c_str());
    fprintf(ll_table, "\n");

    for (auto &p : production_order) {
        fprintf(ll_table, "%s", p.c_str());
        for (auto &column : columns_order) {
            PRODUCTION_AS_STRING(str, values[p][column]);
            fprintf(ll_table, "\t%s", str.c_str());
        }
        fprintf(ll_table, "\n");
    }

}

void Debug::initStackSteps() {
    DEBUG;
    fprintf(stack_steps, "stack\tinput\toutput\n");
}

void Debug::addStackStep(vector<string> &stack) {
    DEBUG;
    PRODUCTION_AS_STRING(str, stack);
    fprintf(stack_steps, "%s", str.c_str());
}

void Debug::finishStackStep(string &token, string &state) {
    DEBUG;
    if (token != $_EOF)
        fprintf(stack_steps, "\t%s $\t%s\n", token.c_str(), state.c_str());
    else
        fprintf(stack_steps, "\t$\t%s\n", state.c_str());
}
