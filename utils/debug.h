#ifndef PHASE2_PARSER_DEBUG_H
#define PHASE2_PARSER_DEBUG_H

#include <iostream>
#include <unordered_map>
#include "../tokenizer/token.h"
#include "../parser/production.h"

#define PRODUCTION_AS_STRING(str, production) \
    string str;\
    for (auto &term : (production))\
        (str) += " " + term;\
    if (!(str).empty())\
        (str) = (str).substr(1)

class Debug {
public:

    static bool isDebug();

    static void activate();

    static void addParsingRule(Production *production);

    static void addBetterParsingRule(Production *production, Production *production_dash);

    static void addToken(Token &token);

    static void printFFTable(unordered_map<string, Production *> &productions);

    static void printLLTable(unordered_map<string, Symbol *> &columns,
                             unordered_map<string, unordered_map<string, vector<string>>> &values);

    static void initStackSteps();

    static void addStackStep(vector<string> &stack);

    static void finishStackStep(string &token, string &state);

};

#endif //PHASE2_PARSER_DEBUG_H
